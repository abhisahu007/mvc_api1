﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVC_API1_Frontend.Models;

namespace MVC_API1_Frontend.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            _logger.LogInformation("Index page called");
            return View();
        }
        /*
                public async Task<ActionResult> StudentResult(Student studentData)
                {
                    string baseUrl = "";
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(baseUrl);
                        var request = await client.GetAsync("api/Student");
                        if (request.IsSuccessStatusCode)
                        {
                            var result = request.Content.ReadAsStringAsync().Result;
                            studentData = JsonConvert.DeserializeObject<StudentV>(result);
                        }
                    }

                    ViewBag.FirstName = studentData.FirstName;
                    ViewBag.LastName = studentData.LastName;
                    ViewBag.Email = studentData.Email;
                    ViewBag.State = studentData.State;
                    ViewBag.Address = studentData.Address;
                    return View("Index",studentData);
                }
        */
        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Student()
        {
            return View();
        }

        public IActionResult HandleStudent(Student data)
        {
            return View("Index",data);
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
