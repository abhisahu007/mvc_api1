﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MVC_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Consumes("application/json")]
    public class StudentController : Controller
    {
        private readonly List<Student> studentList = new List<Student>();

        public StudentController()
        {
            studentList.Add(new Student
            {
                firstName= "Abhi",
                lastName = "Sahu",
                email = "abhi123@gmail.com",
                state = "UP",
                address = "Jaunpur"
            });

            studentList.Add(new Student
            {
                firstName = "Test1",
                lastName = "1",
                email = "test1@gmail.com",
                state = "MP",
                address = "Bhopal"
            });

            studentList.Add(new Student
            {
                firstName = "Test2",
                lastName = "2",
                email = "test2@gmail.com",
                state = "HP",
                address = "Manali"
            });
        }

        [HttpGet]
        [ProducesResponseType(typeof(Student), (int)HttpStatusCode.Accepted)]
        public Task<Student> GetStudent()
        {
            var data = new Student
            {
                firstName = "Abhi",
                lastName = "Sahu",
                email = "abhi123@gmail.com",
                state = "UP",
                address = "Jaunpur"
            };
            return Task.FromResult(data);
        }


       


        [HttpGet]
        [Route("getall")]
        [ProducesResponseType(typeof(Student), (int)HttpStatusCode.Created)]
        public Task<Student> GetAllStudent()
        {
            var data = new Student
            {
                firstName = "Abhi",
                lastName = "Sahu",
                email = "abhi123@gmail.com",
                state = "UP",
                address = "Jaunpur"
            };
            return Task.FromResult(data);
        }

        [HttpDelete("{id:int}")]
        [ProducesResponseType(typeof(Student), (int)HttpStatusCode.Found)]
        public Task<Student> DeleteStudent()
        {
            var data = new Student
            {
                firstName = "Abhi",
                lastName = "Sahu",
                email = "abhi123@gmail.com",
                state = "UP",
                address = "Jaunpur"
            };
            return Task.FromResult(data);
        }


        [HttpPost]
        public Task<Student> AddNewStudent(Student data)
        {
            studentList.Add(data);
            return Task.FromResult(data);
        }

        [HttpPost]
        [ApiVersion("3.0")]
        public Task<Student> AddANewStudent(Student data)
        {
            studentList.Add(data);
            return Task.FromResult(data);
        } 

        public IActionResult Index()
        {
            return View();
        }
    }
}
