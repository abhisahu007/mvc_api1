﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

 namespace MVC_API
   {
    public class Student
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string state { get; set; }
        public string address { get; set; }
    }
}
